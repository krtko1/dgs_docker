FROM base/archlinux:latest

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
COPY latex-enumitem-3.5.2-1-any.pkg.tar.gz /latex-enumitem.pkg.tar.gz
RUN pacman --noconfirm -Sy pacman-contrib && \
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup && \
    sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup && \
    rankmirrors -n 6 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist && \
    pacman --noconfirm -Sy \
    pandoc \
    pandoc-crossref \
    make \
    python \
    python-pip \
    texlive-core \
    texlive-science \
    texlive-fontsextra \
    texlive-latexextra \
    python-yaml \
    ghostscript \
    librsvg \
    gnuplot \
    xorg-mkfontscale \
    xorg-mkfontdir \
    git && \
    pip install jinja2 && \
    yes | pacman -U \
    /latex-enumitem.pkg.tar.gz && \
    echo en_US.UTF-8 UTF-8 > /etc/locale.gen && \
    locale-gen && \
    ln -s /etc/fonts/conf.avail/09-texlive-fonts.conf /etc/fonts/conf.d/09-texlive-fonts.conf && \
    fc-cache && \
    mkfontscale && \
    mkfontdir && \
    git clone https://gitlab.com/krtko1/dgs.git && \
    pacman --noconfirm -Rns git pacman-contrib
